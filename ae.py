# Implementation of auto encoder

import numpy as np
import tensorflow as tf
import tensorflow.python.keras
from tensorflow.python.keras.layers import Input, Dense, GaussianNoise, Lambda, Dropout
from tensorflow.python.keras.models import Model
from tensorflow.python.keras import regularizers
from tensorflow.python.keras.layers import BatchNormalization
from tensorflow.python.keras.optimizers import Adam, SGD
import matplotlib.pyplot as plt

# For reproducing result
# from numpy.random import seed
# seed( 1 )
# from tensorflow import set_random_seed
# set_random_seed( 3 )

# Define parameters
# define (n,k) here for (n,k)-auto encoder
# n = n_channel
# k = log2(M) ==> so for (7,4) autoencoder n_channel = 7 and M = 2^4 = 16 
M = 16
k = np.log2( M )
k = int( k )
n_channel   = 2
R = float(k)/n_channel
print( 'M: ', M, 'k: ', k, 'n: ', n_channel )

# Generate random data
N = 8000
label   = np.random.randint( M, size = N )

# Create one hot encoded vectors
data    = []
for i in label:
    temp    = np.zeros( M )
    temp[i] = 1
    data.append( temp )

data    = np.array( data )
print( data.shape )

# Checking generated data with it's label
temp_check  = [ 17, 23, 45, 67, 89, 96, 72, 250, 350 ]
for i in temp_check:
    print( label[i], data[i] )

input_signal    = Input( shape=(M,) )
encoded         = Dense( M, activation = 'relu' )( input_signal )
encoded1        = Dense( n_channel, activation = 'linear' )( encoded )
# encoded2        = Lambda( lambda x: np.sqrt(n_channel)*tf.nn.l2_normalize(x,1) )( encoded1 )
encoded2        = Lambda( lambda x: tf.nn.l2_normalize(x,1) )( encoded1 )

EbN0_train      = 5.01187
encoded3        = GaussianNoise( np.sqrt(1/(2*R*EbN0_train)) )( encoded2 )

decoded         = Dense( M, activation = 'relu' )( encoded3 )
decoded1        = Dense( M, activation = 'softmax' )( decoded )
autoencoder     = Model( input_signal, decoded1 )

adam    = Adam( lr = 0.01 )
autoencoder.compile( optimizer = adam, loss = 'categorical_crossentropy' )

print( autoencoder.summary() )

autoencoder.fit( data, data, epochs = 45, batch_size = 64  )

# Encoder
encoder = Model( input_signal, encoded2 )
# Decoder
encoded_input   = Input( shape = (n_channel,) )
deco    = autoencoder.layers[-2]( encoded_input )
deco    = autoencoder.layers[-1]( deco )
decoder = Model( encoded_input, deco )

# Generating data for checking BER
N = 50000
test_label  = np.random.randint( M, size = N )
test_data   = []

for i in test_label:
    temp    = np.zeros( M )
    temp[i] = 1
    test_data.append( temp )
test_data   = np.array( test_data )

# Plot learned constellation diagram
scatter_plot    = []
for i in range(0,M):
    temp    = np.zeros( M )
    temp[i] = 1
    scatter_plot.append( encoder.predict(np.expand_dims(temp,axis=0)) )
scatter_plot    = np.array( scatter_plot )

scatter_plot    = scatter_plot.reshape( M, 2, 1)
plt.figure( figsize = (6,6) )
plt.scatter( scatter_plot[:,0], scatter_plot[:,1] )
plt.axis( (-2.5,2.5,-2.5,2.5) )
plt.grid()
plt.show()

# # Calculate BER
# def frange( x, y, jump ):
#     while x < y:
#         yield x
#         x += jump

# EbN0_range  = list( frange(-4,8.5,1.0) )
# ber = [None] * len(EbN0_range)
# for n in range( 0, len(EbN0_range) ):
#     EbN0    = 10.0 ** (EbN0_range[n]/10.0)
#     noise_std   = np.sqrt( 1/(2*R*EbN0) )
#     noise_mean  = 0
#     no_erros    = 0
#     nn          = N
#     noise       = noise_std * np.random.randn( nn, n_channel )
#     encoded_signal  = encoder.predict( test_data )
#     final_signal    = encoded_signal + noise
#     pred_final_signal   = decoder.predict( final_signal )
#     pred_output = np.argmax( pred_final_signal, axis = 1 )
#     no_errors   = (pred_output != test_label)
#     no_errors   = no_errors.astype(float).sum()
#     ber[n]  = no_errors / nn
#     print( 'SNR: ', EbN0_range[n], 'Er:', no_errors, 'BER: ', ber[n] )

# plt.plot( EbN0_range, ber, 'b-', label = 'Autoencoder(2,2)' )
# plt.yscale( 'log' )
# plt.xlabel( 'SNR Range' )
# plt.ylabel( 'Block Error Rate' )
# plt.grid()
# plt.legend( loc = 'upper right', ncol = 1 )
# plt.show()